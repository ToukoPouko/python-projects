import discord
import config
import time
import urllib.request, json 


client = discord.Client()   

@client.event
async def on_ready():
    print("Logged in as {0.user}".format(client))
    
    await client.get_channel(710111869485973878).send(content="JATKAKAA!!!")

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith("tj?"):
        
        left = round((1623920400000 - round(time.time()*1000)) / 1000 / 60 / 60 / 24)
        await message.channel.send("{0}".format(message.author.mention) + ", TJ: " + str(left))
    elif message.content.startswith("muke?"):
        with urllib.request.urlopen("http://ruokalistat.leijonacatering.fi/AromiStorage/blob/menu/339f8191-53ca-ea11-817e-000c29ff1995") as url:
            data = json.loads(url.read().decode())
            print(data)

            embed = discord.Embed(title="title", description="description")
            embed.add_field(name=data.get("Days", {})[0].get("WeekDay", {}), value=data.get("Days", {})[0].get("Meals")[0].get("MealType", {}))
            await message.channel.send(embed=embed)

client.run(config.token)    